﻿using UnityEngine;
using System.Collections;

/// <summary>
/// 摄像机跟随角色移动
/// </summary>
public class CameraFollow : MonoBehaviour {

    private Transform m_transform;
    private Transform m_player;
 
    public bool startFollow = false;

    private Vector3 normalPos; 

    void Start () {
        m_transform = gameObject.GetComponent<Transform>();
        normalPos = m_transform.position;
        m_player = GameObject.Find("cube_car").GetComponent<Transform>();
    }
	
	void Update () {
        CameraMove();
    }

    void CameraMove()
    {
        if(startFollow)
        {
            Vector3 nextPos = new Vector3(m_transform.position.x, m_player.position.y + 1.5f, m_player.position.z);
            //m_transform.position = nextPos;
            //使用差值运算
            //实现平稳的摄像机移动+缓冲效果
            m_transform.position = Vector3.Lerp(m_transform.position, nextPos, Time.deltaTime);
        }
    }

    public void ResetCamare(){
        m_transform.position = normalPos;
    }
}
